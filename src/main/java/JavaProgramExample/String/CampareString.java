package JavaProgramExample.String;

import java.sql.SQLOutput;

public class CampareString {
    public static void main(String[] args) {
       String str = "Hello World";
       String s = "hello world";
       Object o = str;

        System.out.println(str.compareTo(s));//-32
        System.out.println(str.compareToIgnoreCase(s));//0
        System.out.println(str.compareTo(o.toString()));//0

        System.out.println("===========================String compare by equals()======================================================");

        String s1 = "abc";
        String s2 = "abc";
        String s3 = new String("Abc");
        System.out.println(s1.equals(s2));//true
        System.out.println(s2.equals(s3));//false

        System.out.println("==============================String compare by == operator===================================================");

        String sa = "kumargupta";
        String sb = "kumargupta";
        String sc = new String("Kumar Gupta");
        System.out.println(s1 == s2);//true
        System.out.println(s2 == s3);//false


        System.out.println("==============================How to search the last position of a substring ?===================================================");

        String s4 = "Hello world ,Hello is Reader";
        int lastIndex = s4.lastIndexOf("Hello");

        if(lastIndex == -1){
            System.out.println("Hello not found");
        }else {
            System.out.println("Last occurrence of Hello is at index "+ lastIndex);//13
        }

        String t1 = "Tutorialspoint";
        int index = t1.lastIndexOf("p");
        System.out.println(index);//9



        System.out.println("==============================How to remove a particular character from a string ?===================================================");

        String s5 = "this is Java";
        System.out.println(removeCharAt(s5,3));//thi is Java


        System.out.println("==============================How to replace a substring inside a string by another one ?===================================================");

        String st = "Hello World";
        System.out.println(st.replace('H','W'));//Wello World
        System.out.println(st.replaceFirst("He","Wa"));//Wallo World
        System.out.println(st.replaceAll("He","Ha"));//Hallo World

        System.out.println("==============================How to reverse a String?===================================================");

        //method 1
        String s6 = "abcde";
        String revrse = new StringBuffer(s6).reverse().toString();
        System.out.println("\nString before reverse: "+s6);
        System.out.println("String after reverse: "+revrse);

        //method2
        String s7 = "akdkgivcnb";
        char[] chars = s7.toCharArray();
        for(int i = chars.length-1;i>=0;i--)
            System.out.print(chars[i]);


        System.out.println("==============================How to search a word inside a string ?===================================================");


    }//
     //("==============================How to remove a particular character from a string ?===================================================");
    public static String removeCharAt(String s, int pos){
        return s.substring(0,pos)+s.substring(pos+1);
    }
}

/*   String input = "tutorialspoint";
      char[] try1 = input.toCharArray();
      for (int i = try1.length-1;i >= 0; i--) System.out.print(try1[i]);
   }
   }*/