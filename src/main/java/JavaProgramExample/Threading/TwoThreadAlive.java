package JavaProgramExample.Threading;
//How to check a thread is alive or not ?
public class TwoThreadAlive extends Thread{

    @Override
    public void run() {
        for(int i =0;i < 20;i++){
            printMsg();
        }
    }

    public void printMsg(){
        Thread thread = Thread.currentThread();
        String name = thread.getName();
        System.out.println("Name:= "+name);
    }

    public static void main(String[] args) {
        TwoThreadAlive threadAlive = new TwoThreadAlive();
        threadAlive.setName("Thread");
        System.out.println("before start(), threadAlive.isAlive() = " + threadAlive.isAlive());
        threadAlive.start();
        System.out.println("just after start(), threadAlive.isAlive() = "+threadAlive.isAlive());

        for(int i =0; i<10;i++){
            threadAlive.printMsg();
        }
        System.out.println("The end of main(), threadAlive.isAlive() = "+ threadAlive.isAlive());
    }
}