package JavaProgramExample.Threading;

// How to get the priorities of running threads?

public class SimplePriority extends Thread {
    private int coutDown  =5;
    private volatile double d =0;
    public SimplePriority(int priority){
        setPriority(priority);
        start();
    }

    @Override
    public String toString() {
        return super.toString() + ": "+coutDown;
    }

    @Override
    public void run() {

        while (true){
            for(int i  = 1; i<100000;i++)
                d = d + (Math.PI + Math.E) / i;
            System.out.println(this);
            if(--coutDown == 0)
                return;
        }
    }

    public static void main(String[] args) {
        new SimplePriority(Thread.MAX_PRIORITY);
        for(int i = 0;i < 5;i++)
            new SimplePriority(Thread.MIN_PRIORITY);
    }
}