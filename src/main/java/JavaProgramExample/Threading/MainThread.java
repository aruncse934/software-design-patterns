package JavaProgramExample.Threading;
/*How to check a thread has stopped or not ?*/
public class MainThread{
    public static void main(String[] args) throws Exception {
        Thread thread = new MyThread();
        thread.start();

        if(thread.isAlive()){
            System.out.println("Thread has not finished");
        }else {
            System.out.println("Finished");
        }
        long delayMillis = 500;
        thread.join(delayMillis);

        if (!thread.isAlive()){
            System.out.println("thread has not finihed");
        }else {
            System.out.println("Finished");
        }
      thread.join();
    }
}
class MyThread extends Thread {
    boolean stop =false;

    @Override
    public void run() {
        while (true){
            if (stop){
                return;
            }
        }
    }
}