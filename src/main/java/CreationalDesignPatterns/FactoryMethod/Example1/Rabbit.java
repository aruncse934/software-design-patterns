package CreationalDesignPatterns.FactoryMethod.Example1;

public class Rabbit implements Animal{
    @Override
    public void eat() {
        System.out.println("Rabbit is eating, squeak!");
    }
}
