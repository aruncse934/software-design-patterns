package CreationalDesignPatterns.FactoryMethod.Example1;

public class Dog implements Animal {
    @Override
    public void eat() {
        System.out.println("Dog is eating, woof!");
    }
}
