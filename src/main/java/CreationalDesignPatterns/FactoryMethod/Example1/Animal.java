package CreationalDesignPatterns.FactoryMethod.Example1;

public interface Animal {
    void eat();
}
