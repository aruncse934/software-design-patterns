package CreationalDesignPatterns.PrototypePatterns.Example1;

public class Janitor extends Employee {

    public Janitor() {
        position = "Part-Time";
    }

    @Override
    void work() {
        System.out.println("Cleaning the hallway!");
    }
}
