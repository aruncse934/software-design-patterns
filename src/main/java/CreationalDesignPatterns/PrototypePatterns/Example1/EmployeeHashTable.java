package CreationalDesignPatterns.PrototypePatterns.Example1;

import java.util.Hashtable;

public class EmployeeHashTable {
    private static Hashtable<String, Employee> hashtable = new Hashtable<>();

    public static Employee getEmployee(String id){
        Employee cacheEmployee = hashtable.get(id);
        return (Employee) cacheEmployee.clone();
    }

    public static void loadCache(){

        Programmer programmer = new Programmer();
        programmer.setId("ETPN1");
        hashtable.put(programmer.getId(),programmer);

        Janitor janitor = new Janitor();
        janitor.setId("ETJN1");
        hashtable.put(janitor.getId(),janitor);

        Manager manager = new Manager();
        manager.setId("ETMN1");
        hashtable.put(manager.getId(),manager);
    }
}
