package CreationalDesignPatterns.PrototypePatterns.Example1;

public class MainPrototypePattern {
    public static void main(String[] args) {
        EmployeeHashTable.loadCache();
         Employee cloned1 = EmployeeHashTable.getEmployee("ETPN1");
         Employee cloned2 = EmployeeHashTable.getEmployee("ETJN1");
         Employee cloned3 = EmployeeHashTable.getEmployee("ETMN1");

        System.out.println("Employee"+cloned1.getPosition()+"ID"+cloned1.getId());
        System.out.println("Employee"+cloned2.getPosition()+"ID"+cloned2.getId());
        System.out.println("Employee"+cloned3.getPosition()+"ID"+cloned3.getId());

    }
}