package CreationalDesignPatterns.AbstractFactoryPatterns.Example1;

public interface Human {

    public void feedPet();
}
