package CreationalDesignPatterns.AbstractFactoryPatterns.Example1;

public class HumanFactory extends AbstractFactory {

    @Override
    public Pet getPet(String pet) {
        return null;
    }

    @Override
    public Human getHuman(String human) {
        if(human.equals(null))
            return null;
        if(human.equalsIgnoreCase("chILd")){
            return new Child();
        }
        else if(human.equalsIgnoreCase("aDult")){
            return new Adult();
        }
        else if(human.equalsIgnoreCase("elDer")){
            return new Elder();
        }
        return null;
    }
}
