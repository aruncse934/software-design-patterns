package CreationalDesignPatterns.AbstractFactoryPatterns.Example1;

public interface Pet {
    void eat();
}
