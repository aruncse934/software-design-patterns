package CreationalDesignPatterns.AbstractFactoryPatterns.Example1;

public class PetFactory extends AbstractFactory {
    @Override
    public Pet getPet(String pet) {
        if(pet.equals(null))
            return null;
        if(pet.equalsIgnoreCase("CaT")){
            return new Cat();
        }
        else if(pet.equalsIgnoreCase("dOg")){
            return new Dog();
        }
        else if(pet.equalsIgnoreCase("RaBBit")){
            return  new Rabbit();
        }
        return null;
    }

    @Override
    public Human getHuman(String human) {
        return null;
    }
}
