package CreationalDesignPatterns.AbstractFactoryPatterns.Example1;

public class MainAbstractFactory {
    public static void main(String[] args) {
        AbstractFactory humanFactory = AbstractFactoryProducer.getFactory("Human");
        AbstractFactory petFactory = AbstractFactoryProducer.getFactory("Pet");

        Human human = humanFactory.getHuman("Child");
        human.feedPet();

        Pet pet = petFactory.getPet("Dog");
        pet.eat();

        Human human1 = humanFactory.getHuman("Elder");
        human1.feedPet();

        Pet pet1 = petFactory.getPet("Rabbit");
        pet1.eat();
    }
}
