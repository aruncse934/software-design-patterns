package CreationalDesignPatterns.AbstractFactoryPatterns.Example1;

public class Dog implements Pet {
    @Override
    public void eat() {
        System.out.println("Dog is eating, woof!");
    }
}
