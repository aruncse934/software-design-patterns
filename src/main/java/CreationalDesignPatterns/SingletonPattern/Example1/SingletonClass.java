package CreationalDesignPatterns.SingletonPattern.Example1;

public class SingletonClass {
    private static SingletonClass instance = new SingletonClass();

    private SingletonClass(){

    }

    //Eager initialization
    public static SingletonClass getInstancess() {
        return instance;
    }

    public void showMassage(){
        System.out.println(" I'm a singleton Objects");
    }

     //StaticBlockSingleton
        private static SingletonClass instance1;

       // private SingletonClass(){}

        //static block initialization for exception handling
        static{
            try{
                instance1 = new SingletonClass();
                System.out.println("static block initialization for exception handling");
            }catch(Exception e){
                throw new RuntimeException("Exception occured in creating singleton instance");
            }
        }

        public static SingletonClass getInstance1(){
            return instance1;
        }


}


