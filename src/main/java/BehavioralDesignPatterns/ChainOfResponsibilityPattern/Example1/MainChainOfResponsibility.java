package BehavioralDesignPatterns.ChainOfResponsibilityPattern.Example1;

public class MainChainOfResponsibility {
    private static  Employee getChainOfEmployee(){
        Employee programmer = new Programmer(Employee.PROGRAMER);
        Employee leadProgrammer = new LeadProgrammer(Employee.LEAD_PROGRAMER);
        Employee manager = new Manager(Employee.MANAGER);

        programmer.setNextEmployee(leadProgrammer);
        leadProgrammer.setNextEmployee(manager);

        return programmer;
    }

    public static void main(String[] args) {
      Employee employeeChain = getChainOfEmployee();
      employeeChain.doWork(Employee.PROGRAMER,"This is basic programming work.");
      employeeChain.doWork(Employee.LEAD_PROGRAMER,"This is marginally more sophisticated Programming work.");
      employeeChain.doWork(Employee.MANAGER,"This is the work for a manager. ");
    }
}