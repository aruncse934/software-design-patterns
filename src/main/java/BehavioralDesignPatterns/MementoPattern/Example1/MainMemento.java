package BehavioralDesignPatterns.MementoPattern.Example1;

public class MainMemento {
    public static void main(String[] args) {
      Originator originator = new Originator();
      CareTake careTake = new CareTake();

      originator.setState("State 1 at " + System.currentTimeMillis());
      originator.setState("State 2 at " + System.currentTimeMillis());
      careTake.add(originator.saveStateToMemento());

      originator.setState("State 3 at "+ System.currentTimeMillis());
      careTake.add(originator.saveStateToMemento());

      System.out.println("Current state: "+ originator.getState());
      originator.getStateFromMemento(careTake.get(0));
        System.out.println("First saved state: " + originator.getState());
        originator.getStateFromMemento(careTake.get(1));
        System.out.println("Second saved state: "+ originator.getState());
    }
}
