package BehavioralDesignPatterns.MementoPattern.Example1;

import java.util.ArrayList;
import java.util.List;

public class CareTake {
    private List<Memento> mementoList = new ArrayList<>();

    public void add(Memento memento){
        mementoList.add(memento);
    }
    public Memento get(int index){
        return mementoList.get(index);
    }
}
