package BehavioralDesignPatterns.ObserverPattern.Exmple1;

public class Ceo extends Observer {
    public Ceo(Programmer programmer) {
        this.programmer = programmer;
        this.programmer.attach(this);
    }

    @Override
    public void update() {
        if(this.programmer.getState().equalsIgnoreCase("Successful")){
            System.out.println("CEO is happy with Manager and Lead Programmer.");
        }
        else {
            System.out.println("CEO is unhapppy with Manager and Lead Programmer.");
        }

    }
}
