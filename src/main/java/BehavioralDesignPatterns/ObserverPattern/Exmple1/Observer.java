package BehavioralDesignPatterns.ObserverPattern.Exmple1;

public abstract class Observer {
    protected Programmer programmer;
    public abstract void update();
}
