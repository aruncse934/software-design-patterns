package BehavioralDesignPatterns.ObserverPattern.Exmple1;

public class MainOberver {
    public static void main(String[] args) {
      Programmer programmer = new Programmer();

      new Ceo(programmer);
      new LeadProgrammer(programmer);

        System.out.println("Programmer successfully did his job!");
        programmer.setState("Successful");
        System.out.println("Programmer failed his new assignment.");
        programmer.setState("Failed");
    }
}