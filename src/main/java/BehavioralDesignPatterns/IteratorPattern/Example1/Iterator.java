package BehavioralDesignPatterns.IteratorPattern.Example1;

public interface Iterator {
    public boolean hasNext();
    public Object next();
}
