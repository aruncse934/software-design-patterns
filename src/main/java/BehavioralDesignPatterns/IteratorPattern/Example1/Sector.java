package BehavioralDesignPatterns.IteratorPattern.Example1;

public interface Sector {
    public Iterator getIterator();
}
