package BehavioralDesignPatterns.StatePattern.Example1;

public class Context {
    private State state;

    public Context(State state) {
        state = null;
    }

    public Context() {

    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }
}
