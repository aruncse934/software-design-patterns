package BehavioralDesignPatterns.StatePattern.Example1;

public interface State {
    public void doAction(Context context);
}
