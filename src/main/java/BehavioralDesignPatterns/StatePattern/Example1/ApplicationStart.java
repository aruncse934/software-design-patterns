package BehavioralDesignPatterns.StatePattern.Example1;

public class ApplicationStart implements State {
    @Override
    public void doAction(Context context) {

        System.out.println("The applicationis in the starting state of development.");
        context.setState(this);
    }
    public String toString(){
        return "Starting state.";
    }
}
