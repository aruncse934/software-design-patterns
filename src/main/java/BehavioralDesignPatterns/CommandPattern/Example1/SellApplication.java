package BehavioralDesignPatterns.CommandPattern.Example1;

public class SellApplication implements Order {
    private Application application;
    public SellApplication(Application application){
        this.application=application;
    }
    @Override
    public void placeOrder() {
        application.sell();
    }
}
