package BehavioralDesignPatterns.CommandPattern.Example1;

public interface Order {
    void placeOrder();
}
