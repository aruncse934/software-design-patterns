package BehavioralDesignPatterns.MediatorPattern.Example1;

public class Main {
    public static void main(String[] args) {
       User david = new User("David");
       User scott = new User("Scott");
       david.sendMessage("Hi, Scott! how are you?");
       scott.sendMessage("I'm great! thanks for asking. How are you?");
    }
}
