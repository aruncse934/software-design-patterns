package BehavioralDesignPatterns.TemplateMethod.Example1;



public class Programmer extends Employee {

    void work(){
        System.out.println("Writing code.");
    }

    void takePause(){
        System.out.println("Taking a small break from writing code.");
    }
    void getPaid(){
        System.out.println("Getting paid for developing the Project.");
    }
}
