package BehavioralDesignPatterns.InterpreterPattern.Example1;

public interface Expression {
    int interpret(InterpreterEngine engine);
}
