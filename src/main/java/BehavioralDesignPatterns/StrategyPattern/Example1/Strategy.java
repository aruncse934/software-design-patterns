package BehavioralDesignPatterns.StrategyPattern.Example1;

public interface Strategy {
    public String build(String location);
}
