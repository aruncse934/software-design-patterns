package BehavioralDesignPatterns.VisitorPattern.Example1;

public class VisitorImpl implements Visitor {
    @Override
    public int visit(Pen pen) {
        int price = pen.getPrice();
        System.out.println(pen.getModel() + " costs " + price);
        return price;
    }

    @Override
    public int visit(NooteBook nooteBook) {
        int price = 0;
        if(nooteBook.getNumberOfPages() > 250){
            price = nooteBook.getPrice()-5;
        }else {
            price = nooteBook.getPrice();
        }
        System.out.println("NoteBook costs "+ price);
        return price;
    }
}
