package BehavioralDesignPatterns.VisitorPattern.Example1;

public interface Item {
    public int accept(Visitor visitor);
}
