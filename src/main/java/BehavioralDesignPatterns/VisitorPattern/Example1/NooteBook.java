package BehavioralDesignPatterns.VisitorPattern.Example1;

public class NooteBook implements Item{
    private int price;
    private int numberOfPages;

    public NooteBook(int price, int numberOfPages) {
        this.price = price;
        this.numberOfPages = numberOfPages;
    }

    public int getPrice() {
        return price;
    }

    public int getNumberOfPages() {
        return numberOfPages;
    }

    @Override
    public int accept(Visitor visitor) {
        return visitor.visit(this);
    }
}
