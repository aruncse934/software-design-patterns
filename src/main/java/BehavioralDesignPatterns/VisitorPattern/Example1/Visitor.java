package BehavioralDesignPatterns.VisitorPattern.Example1;

public interface Visitor {
    int visit(Pen pen);
    int visit(NooteBook nooteBook);
}
