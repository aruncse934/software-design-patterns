package J2EEPatterns.MVCPattern.Example1;

public class Employee {
    private int employeeId;
    private String name;

    public int getEmployeeId(int i) {
        return employeeId;
    }
    public void setEmployeeId(int id) {
        this.employeeId = id;
    }
    public String getName() {
        return name;
    }
    public void setEmployeeName(String name) {
        this.name = name;
    }

}
