package J2EEPatterns.MVCPattern.Example1;

public class EmployeeView {
  public void printEmployeeInformation(String employeeName, int employeeId){
      System.out.println("Employee Information::");
      System.out.println("ID: "+employeeId);
      System.out.println("Name: "+employeeName);
  }
}
