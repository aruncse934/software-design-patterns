package J2EEPatterns.MVCPattern.Example1;

public class EmployeeController {
    private Employee employee;
    private EmployeeView employeeView;

    public EmployeeController(Employee employee, EmployeeView employeeView) {
        this.employee = employee;
        this.employeeView = employeeView;
    }

    public String getEmployeeName() {
        return employee.getName();
    }

    public void setEmployee(String name) {
        employee.setEmployeeName(name);
    }

    public int  getEmployeeId() {
        return employee.getEmployeeId(1);
    }

    public void  setEmployeeId(int id){
        employee.setEmployeeId(id);
    }
    public void setEmployeeView(EmployeeView employeeView) {
        this.employeeView = employeeView;
    }
    public void updateView(){
        employeeView.printEmployeeInformation(employee.getName(),employee.getEmployeeId(1));
    }

}
