package J2EEPatterns.MVCPattern.Example1;

public class MainMVC {
    public static void main(String[] args) {

        Employee employee = getEmployeeFromDatabase();
        EmployeeView employeeView =  new EmployeeView();
        EmployeeController controller = new EmployeeController(employee,employeeView);
        controller.updateView();
        controller.setEmployeeId(5);
        controller.updateView();
    }

    public static Employee getEmployeeFromDatabase(){
        Employee employee= new Employee();
        employee.setEmployeeName("David");
        employee.getEmployeeId(1);
        return employee;
    }
}
