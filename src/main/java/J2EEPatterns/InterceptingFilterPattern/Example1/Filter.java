package J2EEPatterns.InterceptingFilterPattern.Example1;

public interface Filter {
    public void execute(String request);
}
