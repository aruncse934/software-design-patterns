package J2EEPatterns.InterceptingFilterPattern.Example1;

public class AuthenticationFilter implements Filter {
    @Override
    public void execute(String request) {
        System.out.println("Authentication request: " + request);
    }
}
