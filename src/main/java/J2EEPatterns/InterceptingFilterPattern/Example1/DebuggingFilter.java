package J2EEPatterns.InterceptingFilterPattern.Example1;

public class DebuggingFilter implements Filter {
    @Override
    public void execute(String request) {
        System.out.println("Logging request: " + request);
    }
}
