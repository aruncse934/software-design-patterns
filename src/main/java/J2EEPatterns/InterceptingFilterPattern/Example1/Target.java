package J2EEPatterns.InterceptingFilterPattern.Example1;

public class Target {
    public void execute(String request) {
        System.out.println("Executing request: " + request);
    }
}
