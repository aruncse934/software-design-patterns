package J2EEPatterns.CompositeEntityPattern.Example1;

public class CompositeEntity {
    private CoarseGrainedObject cgo = new CoarseGrainedObject();

    public void setData(String jobSucess,String satisfaction){
        cgo.setData(jobSucess,satisfaction);
    }

    public String[] getData(){
        return cgo.getData();
    }
}