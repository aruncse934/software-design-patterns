package J2EEPatterns.CompositeEntityPattern.Example1;

public class Manager {
    private String name;
    private String satisfaction;

    public String getSatisfaction() {
        return satisfaction;
    }

    public void setSatisfaction(String satisfaction) {
        this.satisfaction = satisfaction;
    }
}
