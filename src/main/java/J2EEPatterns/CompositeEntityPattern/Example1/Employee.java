package J2EEPatterns.CompositeEntityPattern.Example1;

public class Employee {
    private String name;
    private String jobSuccess;

    public String getJobSuccess() {
        return jobSuccess;
    }

    public void setJobSuccess(String jobSuccess) {
        this.jobSuccess = jobSuccess;
    }
}
