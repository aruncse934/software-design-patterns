package J2EEPatterns.CompositeEntityPattern.Example1;

public class Client {
    private CompositeEntity compositeEntity = new CompositeEntity();

    public void print(){
        for(int i = 0; i<compositeEntity.getData().length;i++){
            System.out.println(compositeEntity.getData()[i]);
        }
    }

    public void setData(String jobSucess,String satisfaction){
        compositeEntity.setData(jobSucess,satisfaction);
    }
}
