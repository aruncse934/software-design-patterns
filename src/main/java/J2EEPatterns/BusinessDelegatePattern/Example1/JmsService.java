package J2EEPatterns.BusinessDelegatePattern.Example1;

public class JmsService implements BusinessService {
    @Override
    public void process() {
        System.out.println("Processing using the JSM Service.");
    }
}
