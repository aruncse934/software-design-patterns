package J2EEPatterns.BusinessDelegatePattern.Example1;

public class BusinessLookup {
    public BusinessService grtBusinessService(String type){
        if (type.equalsIgnoreCase("ejb")  ){
            return new EjbService();
        }else if(type.equalsIgnoreCase("JMS")){
            return new JmsService();
        }else {
            return null;
        }
    }
}