package J2EEPatterns.BusinessDelegatePattern.Example1;

public interface BusinessService {
    public void process();
}
