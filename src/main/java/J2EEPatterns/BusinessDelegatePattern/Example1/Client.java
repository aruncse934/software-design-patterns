package J2EEPatterns.BusinessDelegatePattern.Example1;

public class Client {
   BusinessDelegate businessDelegate;

    public Client(BusinessDelegate businessDelegate) {
        this.businessDelegate = businessDelegate;
    }

    public void process(){
        businessDelegate.process();
    }
}
