package J2EEPatterns.BusinessDelegatePattern.Example1;

public class EjbService implements BusinessService {
    @Override
    public void process() {
        System.out.println("Processing using the EJB Service.");
    }
}
/**/