package J2EEPatterns.BusinessDelegatePattern.Example1;

public class BusinessDelegate {
   private BusinessLookup businessLookup = new BusinessLookup();
   private BusinessService businessService;
   private String type;

   public void setServiceType(String type){
       this.type=type;
   }
   public void process(){
       businessService = businessLookup.grtBusinessService(type);
       businessService.process();
   }
}
