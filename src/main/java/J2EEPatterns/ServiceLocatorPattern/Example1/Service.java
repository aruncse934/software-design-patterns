package J2EEPatterns.ServiceLocatorPattern.Example1;

public interface Service {
    public String getServiceName();
    public void execute();
}
