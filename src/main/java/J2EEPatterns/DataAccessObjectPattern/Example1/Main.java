package J2EEPatterns.DataAccessObjectPattern.Example1;

public class Main {
    public static void main(String[] args) {
      EmployeeDao employeeDao = new EmployeeDaoImpl();
      for(Employee employee : employeeDao.getAllEmployees()){
          System.out.println("Employee Info: |Name: " + employee.getName() + ", Id: "+employee.getEmployeeId() + "|");
      }
    }
}
