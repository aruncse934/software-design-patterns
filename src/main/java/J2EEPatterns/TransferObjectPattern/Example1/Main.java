package J2EEPatterns.TransferObjectPattern.Example1;

public class Main {

    public static void main(String[] args) {
        EmployeeBo employeeBo = new EmployeeBo();

        for(EmployeeVo employee : employeeBo.getAllEmployees()){
            System.out.println("Employee: |" + employee.getName() + ", Id: "+ employee.getEmployeeId() + "|");
        }
        EmployeeVo employee = employeeBo.getAllEmployees().get(0);
        employee.setName("Andrew");
        employeeBo.updateEmployee(employee);

        employee = employeeBo.getEmployee(0);
        System.out.println("Employee: |" + employee.getName() +", Id: "+ employee.getEmployeeId() + "|");
    }
}
