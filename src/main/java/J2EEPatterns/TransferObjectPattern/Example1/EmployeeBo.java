package J2EEPatterns.TransferObjectPattern.Example1;


import java.util.ArrayList;
import java.util.List;

public class EmployeeBo {
    List<EmployeeVo> employees;

    public EmployeeBo() {
        employees = new ArrayList<>();
        EmployeeVo david = new EmployeeVo(1, "David");
        EmployeeVo scott = new EmployeeVo(2, "Scott");
        EmployeeVo jessica = new EmployeeVo(3, "Jessica");
        employees.add(scott);
        employees.add(scott);
        employees.add(jessica);
    }

    public void deleteEmployee(EmployeeVo employee) {
        employees.remove(employee.getEmployeeId());
        System.out.println("Employee with Id: " + employee.getEmployeeId() + " was successfully deleted. ");
    }

    public List<EmployeeVo> getAllEmployees() {
        return employees;
    }

    public EmployeeVo getEmployee(int id) {
        return employees.get(id);
    }

    public void updateEmployee(EmployeeVo employee) {
        employees.get(employee.getEmployeeId()).setName(employee.getName());
        System.out.println("Employee with ID: " + employee.getEmployeeId() + " successfully updated.");
    }
}