package J2EEPatterns.FrontControllerPattern.Example1;

public class Main {
    public static void main(String[] args) {

        FrontController frontController = new FrontController();
        frontController.dispatchRequest("MAIN");
        frontController.dispatchRequest("Employee");
    }
}
