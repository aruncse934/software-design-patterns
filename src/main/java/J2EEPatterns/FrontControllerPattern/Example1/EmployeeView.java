package J2EEPatterns.FrontControllerPattern.Example1;

public class EmployeeView {
    public void showView() {
        System.out.println("Showing Employee view.");
    }
}
