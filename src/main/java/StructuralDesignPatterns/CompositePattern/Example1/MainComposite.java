package StructuralDesignPatterns.CompositePattern.Example1;

public class MainComposite {
    public static void main(String[] args) {
        Empolyee empolyee1 = new Empolyee("David","Programmer",1500);
        Empolyee employee2 = new Empolyee("Scott", "CEO", 3000);
        Empolyee employee3 = new Empolyee("Andrew", "Manager", 2000);
        Empolyee employee4 = new Empolyee("Scott", "Janitor", 500);
        Empolyee employee5 = new Empolyee("Juliette", "Marketing", 1000);
        Empolyee employee6 = new Empolyee("Rebecca", "Sales", 2000);
        Empolyee employee7 = new Empolyee("Chris", "Programmer", 1750);
        Empolyee employee8 = new Empolyee("Ivan", "Programmer", 1200);

        employee3.addCoworker(empolyee1);
        employee3.addCoworker(employee7);
        employee3.addCoworker(employee8);

        empolyee1.addCoworker(employee7);
        empolyee1.addCoworker(employee8);

        employee2.addCoworker(employee3);
        employee2.addCoworker(employee5);
        employee2.addCoworker(employee6);

        System.out.println(employee2);
        for(Empolyee empolyee : employee2.getCoworkers()){
            System.out.println(empolyee);

            for(Empolyee empolye:empolyee.getCoworkers()){
                System.out.println(empolye);
            }
        }
    }
}