package StructuralDesignPatterns.CompositePattern.Example1;

import java.util.List;

public class Empolyee {
    private String name;
    private String position;
    private int wage;
    private List<Empolyee> coworkers;

    public Empolyee(String name, String position, int wage) {
        this.name = name;
        this.position = position;
        this.wage = wage;
        this.coworkers = coworkers;
    }

    public void addCoworker(Empolyee employee) {
        coworkers.add(employee);
    }

    public void removeCoworker(Empolyee employee) {
        coworkers.remove(employee);
    }

    public List<Empolyee> getCoworkers() {
        return coworkers;
    }

    @Override
    public String toString() {
        return "Empolyee : | Name: "  + name + ", Position : " + position + ", Wage : " + wage + " |";
    }
}