package StructuralDesignPatterns.DecoratorPattern.Example1;

public interface Computer {
    void assemble();
}
