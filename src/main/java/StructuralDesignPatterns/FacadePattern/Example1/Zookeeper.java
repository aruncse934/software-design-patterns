package StructuralDesignPatterns.FacadePattern.Example1;

public class Zookeeper {
    private Animal lion;
    private Animal wolf;
    private Animal bear;

    public Zookeeper() {
        lion = new Lion();
        wolf = new Wolf();
        bear = new Bear();
    }

    public void feedLion() {
        lion.feed();
    }

    public void feedWolf() {
        wolf.feed();
    }

    public void feedBear() {
        bear.feed();
    }
}
