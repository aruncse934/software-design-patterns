package StructuralDesignPatterns.FacadePattern.Example1;

public class Lion implements Animal {
    @Override
    public void feed() {
        System.out.println("The lion is being fed!");
    }
}
