package StructuralDesignPatterns.FacadePattern.Example1;

public class MainFacede {
    public static void main(String[] args) {
        Zookeeper zookeeper = new Zookeeper();
        zookeeper.feedLion();
        zookeeper.feedWolf();
        zookeeper.feedBear();

    }
}
