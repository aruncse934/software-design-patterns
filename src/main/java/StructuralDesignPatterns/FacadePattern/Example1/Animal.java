package StructuralDesignPatterns.FacadePattern.Example1;

public interface Animal {
    void feed();
}
