package StructuralDesignPatterns.FlyweightPattern.Example1;

import java.util.HashMap;

public class AttendeeFactory {
    private static final HashMap attendees = new HashMap();

    public static Attendee getAttendees(String name){
        AttendeeImpl attendeeImpl = (AttendeeImpl) attendees.get(name);
        if(attendeeImpl == null){
            attendeeImpl = new AttendeeImpl(name);
            attendees.put(name, attendeeImpl);
            System.out.println("Creating a new attendee: " + name);
        }
        return attendeeImpl;
    }
}
