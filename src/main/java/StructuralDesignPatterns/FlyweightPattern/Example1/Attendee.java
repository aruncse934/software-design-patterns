package StructuralDesignPatterns.FlyweightPattern.Example1;

public interface Attendee {
    public void listenToConcert();
}
