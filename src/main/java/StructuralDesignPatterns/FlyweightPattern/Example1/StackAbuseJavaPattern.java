package StructuralDesignPatterns.FlyweightPattern.Example1;

import java.util.Random;

public class StackAbuseJavaPattern {
    private static final String[] names = {"David","Scott","Andrew","Rhett"};

    public static void main(String[] args) {
        for (int i =0 ;i < 10;i++){
            AttendeeImpl attendeeImpl = (AttendeeImpl) AttendeeFactory.getAttendees(getRandomName());
            attendeeImpl.setAge(getRamdomAge());
            attendeeImpl.listenToConcert();
        }
    }

    private static String getRandomName(){
        int randomName = new Random().nextInt(names.length);
        return names[randomName];
    }
    private static int getRamdomAge(){
        return (int) (Math.random()*80);
    }
}
