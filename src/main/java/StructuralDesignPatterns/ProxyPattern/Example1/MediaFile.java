package StructuralDesignPatterns.ProxyPattern.Example1;

public interface MediaFile {
    void printName();
}
