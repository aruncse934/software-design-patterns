package StructuralDesignPatterns.FilterPattern.Example1;

import java.util.ArrayList;
import java.util.List;

public class CriteriaJunior implements Criteria {

    @Override
    public List<Employee> criteria(List<Employee> employeeList) {
        List<Employee> juniorEmployee = new ArrayList<>();
        for(Employee employee : juniorEmployee){
            if(employee.getPosition().equalsIgnoreCase("Junior")){
                juniorEmployee.add(employee);
            }
        }
        return juniorEmployee;
    }
}
