package StructuralDesignPatterns.FilterPattern.Example1;

import java.util.ArrayList;
import java.util.List;

public class CriteriaFemale implements Criteria {
    @Override
    public List<Employee> criteria(List<Employee> employeeList) {
        List<Employee> femaleEmployee  = new ArrayList<>();
        for(Employee employee : femaleEmployee){
            if(employee.getGender().equalsIgnoreCase("Female")){
                femaleEmployee.add(employee);
            }
        }
        return femaleEmployee;
    }
}
