package StructuralDesignPatterns.FilterPattern.Example1;

import java.util.ArrayList;
import java.util.List;

public class CriteriaSenior implements Criteria {
    @Override
    public List<Employee> criteria(List<Employee> employeeList) {
        List<Employee> seniorEmployee = new ArrayList<>();

        for(Employee employee : seniorEmployee){
            if(employee.getPosition().equalsIgnoreCase("Senior")){
                seniorEmployee.add(employee);
            }
        }
        return seniorEmployee;
    }
}
