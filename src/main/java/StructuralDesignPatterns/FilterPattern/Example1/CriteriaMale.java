package StructuralDesignPatterns.FilterPattern.Example1;

import java.util.ArrayList;
import java.util.List;

public class CriteriaMale implements Criteria {
    @Override
    public List<Employee> criteria(List<Employee> employeeList) {
        List<Employee> maleEmployee = new ArrayList<>();
        for(Employee employee : maleEmployee){
            if(employee.getGender().equalsIgnoreCase("Male")){
                maleEmployee.add(employee);
            }
        }
        return maleEmployee;
    }
}