package StructuralDesignPatterns.FilterPattern.Example1;

import java.util.List;

public class OrCriteria implements Criteria {

    private Criteria firstCriteria;
    private Criteria secondCriteria;

    public OrCriteria(Criteria firstCriteria, Criteria secondCriteria) {
        this.firstCriteria = firstCriteria;
        this.secondCriteria = secondCriteria;
    }

    @Override
    public List<Employee> criteria(List<Employee> employeeList) {
        List<Employee> firstCriteriaEmployee = firstCriteria.criteria(employeeList);
        List<Employee> secondCriteriaEmployee = secondCriteria.criteria(employeeList);

        for(Employee employee : secondCriteriaEmployee){
            if(!firstCriteriaEmployee.contains(employee)){
                firstCriteriaEmployee.add(employee);
            }
        }
        return firstCriteriaEmployee;
    }
}
