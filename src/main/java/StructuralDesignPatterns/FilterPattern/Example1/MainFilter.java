package StructuralDesignPatterns.FilterPattern.Example1;

import java.util.ArrayList;
import java.util.List;

public class MainFilter {
    public static void main(String[] args) {
        List<Employee> employeeList = new ArrayList<>();
        employeeList.add(new Employee("David","Male","Senior"));
        //employeeList.add(new Employee("arun","male","senior" ));
        employeeList.add(new Employee("Scott", "Male", "Senior"));
        employeeList.add(new Employee("Rhett", "Male", "Junior"));
        employeeList.add(new Employee("Andrew", "Male", "Junior"));
        employeeList.add(new Employee("Susan", "Female", "Senior"));
        employeeList.add(new Employee("Rebecca", "Female", "Junior"));
        employeeList.add(new Employee("Mary", "Female", "Junior"));
        employeeList.add(new Employee("Juliette", "Female", "Senior"));
        employeeList.add(new Employee("Jessica", "Female", "Junior"));
        employeeList.add(new Employee("Mike", "Male", "Junior"));
        employeeList.add(new Employee("Chris", "Male", "Junior"));

        Criteria maleEmployee = new CriteriaMale();
        Criteria femaleEmployee = new CriteriaFemale();
        Criteria seniorEmployee = new CriteriaSenior();
        Criteria juniorEmployee = new CriteriaJunior();
        //arguments and return filtered lists
        Criteria seniorFemale = new AndCriteria(seniorEmployee,femaleEmployee);
        Criteria juniorOrMale = new OrCriteria(juniorEmployee,maleEmployee);

        System.out.println("Male employees: ");
        printEmployeeInfo(maleEmployee.criteria(employeeList));

        System.out.println("\nFemale employee: ");
        printEmployeeInfo(femaleEmployee.criteria(employeeList));

        System.out.println("\nSenior female employee: ");
        printEmployeeInfo(seniorFemale.criteria(employeeList));

        System.out.println("\nJunior or male employee: ");
        printEmployeeInfo(juniorOrMale.criteria(employeeList));
    }

    private static void printEmployeeInfo(List<Employee> employeeList) {
        for(Employee employee : employeeList){
            System.out.println("Employee info: | Name: " + employee.getName() +", Gender: "+employee.getGender() + ", Position: " + employee.getPosition());
        }
    }
}

