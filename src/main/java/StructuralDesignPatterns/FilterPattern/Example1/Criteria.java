package StructuralDesignPatterns.FilterPattern.Example1;

import java.util.List;

public interface Criteria {
    public List<Employee> criteria(List<Employee> employeeList);
}