package StructuralDesignPatterns.BridgePattern.Example1;

public interface FeedingAPI {
    public void feed(int timeADay,int amount, String typeOfFood);
}
