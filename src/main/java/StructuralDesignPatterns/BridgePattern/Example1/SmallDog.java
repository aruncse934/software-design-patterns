package StructuralDesignPatterns.BridgePattern.Example1;

public class SmallDog implements  FeedingAPI {
    @Override
    public void feed(int timeADay, int amount, String typeOfFood) {
        System.out.println("Feeding a small dog, "+ timeADay + " times a day with " + amount + " g of " + typeOfFood);
    }
}
