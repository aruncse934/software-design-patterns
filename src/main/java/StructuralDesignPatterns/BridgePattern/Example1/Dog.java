package StructuralDesignPatterns.BridgePattern.Example1;

public class Dog extends Animal {

    private int timesADay;
    private int amount;
    private String typeOfFood;

    public Dog(FeedingAPI feedingAPI, int timesADay, int amount, String typeOfFood) {
        super(feedingAPI);
        this.timesADay = timesADay;
        this.amount = amount;
        this.typeOfFood = typeOfFood;
    }

    @Override
    public void feed() {
       feedingAPI.feed(timesADay,amount,typeOfFood);
    }
}
