package StructuralDesignPatterns.AdapterPattern.Example1;

public class BuilderAdapter implements Builder {
    private AdvancedBuilder advancedBuilder;

    public BuilderAdapter(String type){
        if(type.equalsIgnoreCase("House")){
            advancedBuilder = new HouseBuilder();
        }else if(type.equalsIgnoreCase("Skyscrapper")){
            advancedBuilder = new SkyscrapperBuilder();
        }
    }

    public void build(String type, String location){
        if(type.equalsIgnoreCase("House")){
            advancedBuilder.buildHouse(location);
        }else if(type.equalsIgnoreCase("Skyscapper")){
            advancedBuilder.buildSkyscrapper(location);
        }
    }
}