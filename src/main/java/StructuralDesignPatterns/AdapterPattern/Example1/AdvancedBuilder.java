package StructuralDesignPatterns.AdapterPattern.Example1;

public interface AdvancedBuilder {

    public void buildHouse(String location);
    public void buildSkyscrapper(String location);
}
