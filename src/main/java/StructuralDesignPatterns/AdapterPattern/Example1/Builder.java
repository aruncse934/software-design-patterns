package StructuralDesignPatterns.AdapterPattern.Example1;

public interface Builder {
 public void build(String type, String location);
}
