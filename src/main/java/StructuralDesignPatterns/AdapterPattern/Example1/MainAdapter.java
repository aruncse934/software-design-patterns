package StructuralDesignPatterns.AdapterPattern.Example1;

public class MainAdapter {
    public static void main(String[] args) {
  BuilderImplementaions  builderImplementaions = new BuilderImplementaions();
  builderImplementaions.build("house","Downtown");
  builderImplementaions.build("Skyscapper","City Center");
  builderImplementaions.build("Skyscapper","Outskirts");
  builderImplementaions.build("Hotel","City Center");
    }
}
